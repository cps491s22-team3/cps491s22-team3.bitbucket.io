University of Dayton

Department of Computer Science

CPS 490 - Fall 2021

Dr. Phu Phung


## Capstone II Proposal 


# Phishing Email Sentiment Analysis


# Team members



1.  John (Jack) Conroy, conroyj4@udayton.edu
2.  Will Manzella, manzellaw2@udayton.edu
3.  Jon Moran, moranj13@udayton.edu
4.  James Oei, oeij01@udayton.edu


# Company Mentors

Jeffrey Archer

Senior Staff Cybersecurity Researcher

GE Aviation

Cincinnati, Ohio

## Project homepage

[Team 3 Website](https://cps491s22-team3.bitbucket.io)


# Overview

![Overview Architecture](https://i.ibb.co/kxMBNFG/Sentiment-Analysis.png)

Figure 1. - A Sample of Overview Architecture of the proposed project.

# Project Context and Scope


The GE Aviation Cyber Intelligence, Active Defense, and Enterprise Vulnerability Management Team currently uses a data engineering framework, dubbed “Magnus”, to extract features from phishing emails sent to GE Employees and use these to score how “suspicious” an email appears using automation.

The Magnus framework is currently used to calculate a suspiciousness score automatically by translating some common patterns looked for by human analysts in email content to automated “signatures” or “feature patterns”. 

While this kind of automation works well with email metadata and header data, it would be optimal to also include information about the message contents, which the Magnus framework does not examine today.

One particularly valuable feature set that can be extracted from email message contents is the sentiments of the message – the overall opinions or emotions underlying the phrases used in a document’s text. Sentiment Analysis of email contents will be the topic of this project and will be used to augment the current Magnus framework by adding features based on the text contents and sentiments of email data.

This project will be able to provide an analysis of sentiment from an email to provide an awareness to the possible intention of the email (e.g. phishing).


# High-level Requirements

1. Create Base NLP Algorithm
2. Find, Clean, and Format Email Data
3. Create Testing Dataset
4. Determine the polarity of given text
5. Implement the Advanced Emotional State Algorithm
6. Implement Phrase Recognition
7. Implement Spell Check Dictionary


# Technology

Jupyter Notebooks: Allows for simple segmented and easily testable python scripts

Python Natural Language Processing: Breaks down sentences into its most identifiable components

Kaggle: Provides thousands of useful datasets, including email and sentiment data

Textblob: Sentiment analysis library 


# Impacts


Sentiment Analysis is the computational study of people’s opinions, sentiments, emotions, and attitudes. It often involves employing Natural Language Processing (NLP) techniques to tokenize the words of a document and analyze the content and context of the text to determine – on a basic level - the polarity of a given text (whether the expressed opinion is positive, negative, or neutral) and – on an advanced level - emotional states such as enjoyment, anger, disgust, sadness, fear, and surprise.

The goal of this project is to develop a sentiment analysis tool that, given the input of the contents of an email, can analyze the email text contents and determine the emotional states present in the text.

The output of the project should include the detected emotional states, and, if possible, which tokens/phrases contributed to the detection of that emotional state. See the sample input and output provided below.

The emotional states detected by the project can include any number of emotions (standard emotions used in sentiment analysis are Happiness, Sadness, Fear, Anger, Surprise, Scare, Shame, and Disgust). Of particular interest for this project are emotions having to do with shaming, intimidation, or harassment, as this category of emotions is often used in phishing emails to influence a victim to complete an action, e.g.:

• “Please click on this link immediately to view your banking statement.”

• “This request requires your urgent attention”

• “I need you to change the banking information ASAP”

• “If you do not respond in 24 hours, we will release this embarrassing information about
you”

• “Change your password here before your account is deleted!”


The technology we'll use — Jupter Notebooks, Python Natural Language Processing, Kaggle, and Textblob — will directly allow us to complete this project and hopefully exceed expectations.



# Project Management

We'll follow the Scrum and Agile methodologies approach throughout the development of this project. We'll use Trello to stay organized and maintain our productivity. Multiple team meetings will take place throughout each week of the Spring semester.

Sprint Outlook:

Sprints 1-4

    These Sprints are focused on building the framework necessary to read the text of emails and set up Natural Language Processing

        Sprint 1: Create Base NLP Algorithm
        Sprint 2: Find Email Data
        Sprint 3: Clean and Format Email Data
        Sprint 4: Create Testing Dataset

Sprints 5-8

    These Sprints are focused on implementing sentiment analysis into the framework of previous 3 sprints
    
        Sprint 5: Determine the polarity of given text
        Sprint 6: Design Advanced Emotional State Algorithm
        Sprint 7: Implement Advanced Emotional State Algorithm
        Sprint 8: Test Advanced Emotional State Implementation

Sprints 9-10

    If all goes well in Sprints 4-7 then we would like to add some advanced features to optimize the program. 
    
        Sprint 9: Implement Phrase Recognition
        Sprint 10: Implement Spell Check Dictionary

Sprints 11-12

    These Sprints are reserved for extra time needed for any unforeseen blocks that we may encounter.
    This is a time to optimize and clean our coding.



Tentative Meeting Schedule:

    Monday: 5-5:30pm
    Wednesday: 5-5:30pm
	Friday: 5-5:30pm

![Spring 2022 Timeline](https://i.ibb.co/N2ZDVkF/trellosc.png)



# Company Support

We'll be in contact with our mentor, Jeff Archer, throughout the entirety of this project. It has been made clear that Jeff make time available to answer any questions, help with any issues, and we'll meet with him a few times throughout the project's duration to check-in and provide updates. Jeff provided his contact information and made sure that if we needed anything or particular guidance (in or out of the project) we can feel free to reach out.